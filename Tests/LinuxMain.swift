import XCTest

import DefaultsTests

var tests = [XCTestCaseEntry]()
tests += DefaultsTests.__allTests()

XCTMain(tests)
